<?php
/*
 * lsc_request accepts a unique identifier from the user and reutrns three pieces of information:
* 1) The next departure time of a shuttle from WTC
* 2) The estimated number of people in line currently
* 3) Whether or not the user has already declared himself to be in line
*/
$response = array();

$timeFile = "LSCTime.txt"; //holds next departure time
$visitFile = "LSCvisit.txt"; //holds list of people currently in line
$notPresent = true; 

$readTime = fopen($timeFile, 'r');
$nextTime = fread($readTime, filesize($timeFile));
fclose($readTime);

$response["nextTime"] = $nextTime;

$visitors = file($visitFile);
$inLine = count($visitors); //number of visitors in line

$response["inLine"] = $inLine;
//TODO - change instances of "IP" to "ID"
if(isset($_POST["IP"])){
	
	$ip = $_POST["IP"];
	
	foreach($visitors as $visitor){
		if(strcmp(trim($ip),trim($visitor))==0){
			$notPresent = false;
		}
	}
}

$response["notPresent"] = $notPresent;

echo json_encode($response);

