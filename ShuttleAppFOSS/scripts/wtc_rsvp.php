<?php
/*
 * wtc_rsvp accepts a unique identifier from the user.  if this person is not already in line, it adds them
 * to the list of people currently in line.
 */

$response = array();
$notPresent = true; //assume initially not in line.

//TODO - change instances of "IP" to "ID"
if(isset($_POST["IP"])){

	$ip = $_POST["IP"]; //unique identifier of user

	$visitFile = "WTCvisit.txt"; //list of people currently in line
	$visitors = file($visitFile);

	//check if person is already present in list of visitors
	foreach($visitors as $visitor){
		if(strcmp(trim($ip),trim($visitor))==0){
			$notPresent = false;
		}
	}
	
	if($notPresent){
		$appendVisitor = fopen($visitFile, 'a');
		fwrite($appendVisitor, "$ip\r\n"); //write new visitor into list of visitors
		fclose($appendVisitor);
		$notPresent = false;
	}
	
	$response["message"] = "success";
	$response["notPresent"] = $notPresent;
}
else{
	$response["message"] = "failure";
	$response["notPresent"] = $notPresent;
}

echo json_encode($response);