<?php
/*
 * change_wtc_time accepts a string and writes this to a txt file.
* This string serves as next departure time of shuttle
*/
$timeFile = "WTCTime.txt"; //holds next departure time
$visitFile = "WTCvisit.txt"; //holds list of people currently in line for shuttle

if(isset($_POST["nextTime"])){
	$nextTime = $_POST["nextTime"];
	
	$writeTime = fopen($timeFile, 'w');
	fwrite($writeTime, $nextTime);
	fclose($writeTime);
	
	$clearVisitors = fopen($visitFile, 'w');//Once time is changed, clear list of people currently in line
	
}
else{
	$nextTime = 0;
}

$message = "Time successfully changed to $nextTime";

echo $message;