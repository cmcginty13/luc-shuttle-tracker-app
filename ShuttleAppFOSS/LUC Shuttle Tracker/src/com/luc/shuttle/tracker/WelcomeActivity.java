package com.luc.shuttle.tracker;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class WelcomeActivity extends Activity {
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_welcome, menu);
        return true;
    }
    
    /*
     * onLSCPress is responsible for taking the user to the LAKE Shore section of the application
     * upon press of the Lake Shore button.
     */
    public void onLscPress(View v){
    	Intent lscIntent = new Intent(WelcomeActivity.this, LSCActivity.class);
    	startActivity(lscIntent);
    	
    }
    
    /*
     * onWtcPress is responsible for taking the user to the Water Tower section of the application
     * upon press of the Water Tower button.
     */
    public void onWtcPress(View v){
    	Intent wtcIntent = new Intent(WelcomeActivity.this, WTCActivity.class);
    	startActivity(wtcIntent);
    }
}
