package com.luc.shuttle.tracker;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.luc.shuttle.tracker.WTCActivity.GetNextTime;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;
/*
 *  LSCActivity handles all sending and retrieving of information for shuttles departing from the Lake Shore campus.
 *  The activity moderates all interaction between user, device, and server.  Allows the user to both retrieve information
 *  about departing shuttles, as well as make their presence in line known so that crowd size can be estimated.
 *  
 *  LSCActivity and WTCActivity are analagous.  One simply allows access to information about Lake Shore shuttles, the other
 *  allows access to information about Water Tower shuttles.
 */
public class LSCActivity extends Activity {
	
	private ProgressDialog pdialog; //this will restrict user access to the UI while httpRequests are being made
	
	TextView timeView;
	TextView crowdView;
	Button rsvp;
	
	String nextTime; //Departue Time of Next Shuttle
	int inLine; //The number of people in line (who have rsvp'd)
	boolean notPresent; //true if the user has not rsvp'd
	String deviceId; //serves as a unique identifier for device
	
	JSONParser jparser = new JSONParser();
	
	String url_get_next = "http://10.0.2.2/luc_shuttle_server/lsc_request.php";
	String url_rsvp = "http://10.0.2.2/luc_shuttle_server/lsc_rsvp.php";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lsc);
        
        timeView = (TextView) findViewById(R.id.shuttleTimeView);
        crowdView = (TextView) findViewById(R.id.crowdView);
        rsvp = (Button) findViewById(R.id.rsvpButton);
        deviceId = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
        
        if(checkConnectivity()){
        	new GetNextTime().execute();
        }
        else{
        	Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
        
    }
	
	/*
	 * Handles the functionality of "I'm Here" button
	 */
	public void onRSVP(View v){
		if(checkConnectivity()){
			new RSVPForShuttle().execute();
        }
        else{
        	Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
	}
	
	/*
	 * Handles functionality of "Refresh" button
	 */
	public void onRefresh(View v){
		if(checkConnectivity()){
        	new GetNextTime().execute();
        }
        else{
        	Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
	}
	
	/*
	 * updateUi simply updates the components of the UI after their values have changed, OR a new http request
	 * is made.
	 */
	public void updateUi(){
		timeView.setText("Next Departure: " + nextTime);
		crowdView.setText("Crowd: " + inLine);
		rsvp.setEnabled(notPresent);
	}
	
	/*
	 * checkConnectivity simply checks whether or not there is an internet connection available for the application to use.
	 * Returns true if a connection is found.  Used before making an httpRequest.
	 */
	public boolean checkConnectivity(){
		ConnectivityManager cMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = cMan.getActiveNetworkInfo();
		
		return activeNetworkInfo != null;		
	}
	
	/*
	 * GetNextTime executes an http request on url_get_next to retreive departure time information.
	 * After getting information from the JSONObject it updates the UI.
	 */
	class GetNextTime extends AsyncTask<String,String,String>{
		JSONObject jObj;
		
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
			pdialog = new ProgressDialog(LSCActivity.this);
			pdialog.setMessage("Loading.  Please Wait...");
			pdialog.setIndeterminate(false);
			pdialog.setCancelable(false);
			pdialog.show();
		}
		
		@Override
		protected String doInBackground(String... args) {
			jObj = null;
			
			List<NameValuePair> getNextParams = new ArrayList<NameValuePair>();
			getNextParams.add(new BasicNameValuePair("IP", deviceId));
			
			try{
				jObj = jparser.httpPostRequest(url_get_next, getNextParams);
				
				if(jObj==null){ //If jObj == null, give the variables some default values-> Lets the user know something is wrong.
					nextTime = "N/A";
					inLine = 0;
					notPresent = true;
					return null;
				}
				
				nextTime = jObj.getString("nextTime");
				inLine = jObj.getInt("inLine");
				notPresent = jObj.getBoolean("notPresent");
			}
			catch(Exception e){
				Log.e("Parse error", e.toString());
			}
			
			return null;
		}
		@Override
		protected void onPostExecute(String file_url){
			pdialog.dismiss();
			
			if(jObj == null){
				Toast.makeText(getApplicationContext(), "Error connecting to server.", Toast.LENGTH_SHORT).show();
			}
			
			updateUi();
			pdialog = null;
		}
	}
	
	/*
	 * RSVPForShuttle allows the user to notify others of their presence in line via an http request
	 * to url_rsvp  
	 */
	class RSVPForShuttle extends AsyncTask<String, String, String>{
		JSONObject jObj;
		
		@Override
		protected void onPreExecute(){
			pdialog = new ProgressDialog(LSCActivity.this);
			pdialog.setMessage("Loading.  Please Wait...");
			pdialog.setIndeterminate(false);
			pdialog.setCancelable(false);
			pdialog.show();
		}
		
		@Override
		protected String doInBackground(String... params) {
			jObj = null;
			
			List<NameValuePair> RSVPparams = new ArrayList<NameValuePair>();
			RSVPparams.add(new BasicNameValuePair("IP", deviceId));
			
			try{
				jObj = jparser.httpPostRequest(url_rsvp, RSVPparams);
				
				if(jObj==null){ //If jObj == null, give the variables some default values-> Lets the user know something is wrong.
					nextTime = "N/A";
					inLine = 0;
					notPresent = true;
					return null;
				}
				
				notPresent = jObj.getBoolean("notPresent");
			}
			catch(Exception e){
				Log.e("Parse error", e.toString());
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String file_url){
			pdialog.dismiss();
			
			if(jObj == null){
				Toast.makeText(getApplicationContext(), "Error connecting to server.", Toast.LENGTH_SHORT).show();
			}
			
			updateUi();
			pdialog = null;
		}
	}
}
