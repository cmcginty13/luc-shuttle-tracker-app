package com.luc.shuttle.tracker;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;

public class JSONParser {
	static InputStream is = null;
	static String json = null;
	static JSONObject jObj = null;
	
	public JSONParser(){
		
	}
	
	/*
	 * ping allows the application to check if the url is available before making an httpRequest.
	 */
	protected boolean ping(String url){
		try{
			URL toTest = new URL(url);
			HttpURLConnection urlc = (HttpURLConnection) toTest.openConnection();
		    urlc.setRequestProperty("User-Agent", "Android Application");
		    urlc.setRequestProperty("Connection", "close");
		    urlc.setConnectTimeout(1000 * 10); //Ten second timeout
		    
		    if (urlc.getResponseCode() == 200) {
		        Log.d("Success", "getResponseCode == 200");
		        return Boolean.valueOf(true);
		    }
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return Boolean.valueOf(false);
	}
	
	public JSONObject httpPostRequest(String url, List<NameValuePair> params){
		is = null;
		json = null;
		jObj = null;
		
		//If url is unavailable, then return null.
		if(!ping(url)){
			return null;//null => error connecting to server
		}
		
		//making http request
		try{
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(new UrlEncodedFormEntity(params));
			
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
		//read from the input stream
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while((line = reader.readLine()) != null){
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
		}
		catch (Exception e){
			Log.e("Buffer error", e.toString());
			e.printStackTrace();
		}
		
		//convert read string to JSONObject
		try {
			jObj = new JSONObject(json);
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
		
		return jObj;
	}	
}
