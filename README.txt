	LUC Shuttle tracker is an application designed to keep track of departing
shuttle times on both campuses at Loyola University Chicago.  The app is also
designed to estimate how full the bus is based on user feedback.  It is free and
easy-to-use.  LUC Shuttle tracker is currently under development and not ready for
widespread use on Android devices.  LUC Shuttle Tracker runs on versions of android
greater than 2.2.

Version 0.1 (12/4/2012)
	-Deliver departure times to user via web-service.
	-Allow user to signal their presence in line to estimate crowd.
	-Establish groundwork for communication between emulator and 
		localhost.

Version 0.2 (in progress)
	-Create a full time server for storage of departure times and crowd data.
	-Establish groundwork for communication between android device and
		full time server.
	-Design a more flexible UI that can run on multiple devices.

Version 1.0 and Beyond
	-Establish connection between server and devices responsible for 
		displaying upcoming departure times.**

LUC Shuttle Tracker is an open-source project liscensed under:
	Creative Commons Attribution-Non Commercial 3.0 Unported
	http://creativecommons.org/licenses/by-nc/3.0/deed.en_US

Those interested in contributing to this project should consult one of the current
members of the project (information located in credits file).  Furthermore, those
who are ready to contribute should consult the issues section of the repository and
declare their intent to resolve the issue.

**This is the biggest issue hindering the development of this project.  Once
	a method for getting input from these devices is determined the avail-
	ability of the app should follow shortly thereafter.
	